This project is licensed under GPL-2.0.  For a copy of a the license, please
see the LICENSE file.

Users making changes must include a "Signed-off-by:" tag on all commits that
acknowledges the DCO, https://developercertificate.org.

There are additional tools maintained by various developers within Red
Hat that may also be of interest.

Included Tools
==============
- **subsys-commits.py** gets a list of commits to backport for the given
  subsystem. [The documentation.](docs/subsys-commits.md)
- kernel-auto-bisect an automated git bisect tool to locate the first
  bad commit or kernel version [The documentation.](kernel-auto-bisect/README.md)

Additional Tools
================
- **rhpatchreview** is a python and dialog based cli patch review tool that accepts
  merge request URLs or Red Hat project names as it's primary input, and walks
  the user through review of the given merge request, with assorted automated
  checks for bug data, upstream commit references, per-patch vimdiff viewing
  options, and an interface for leaving feedback on the merge request, among
  other features.

  https://gitlab.com/jwilsonrh/rhpatchreview

- **igrab.py** is a command-line python tool to make the retrieval of testing results
  easier for kernel maintainers.  Currently, a standard workflow for a kernel
  maintainer is to look translate from dist git --> brew --> retrieve nvr/taskID
  --> OSCI --> open tabs from CKIDB/OSCI artifacts.  It is also necessary for
  maintainers to verify they are investigating the correct build(s), identify
  testcase names, and investigate build or test logs.  igrab.py achieves the same
  workflow with a simple execution of 'igrab -n $NVR --verbose' which prints out
  the testcase names, URLs, and endpoint references.

  https://gitlab.com/debarbos/igrab/-/tree/main
